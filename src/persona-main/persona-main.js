import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-dm/persona-dm.js';

class PersonaMain extends LitElement{

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            maxYearsInCompanyFilter: {type: Number}
        };
    }

    constructor() {
        super();
        this.people = [];
        this.showPersonForm = false;
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <persona-dm @set-people="${this.setPeople}"></persona-dm>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.filter(
                    person => person.yearsInCompany <= this.maxYearsInCompanyFilter
                )
                .map(
                    person => html`<persona-ficha-listado 
                        fname="${person.name}"
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"  ></persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                <persona-form id="idPersonForm" class="d-none border rounded border-primary"
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}" ></persona-form>
            </div>
        `;
    }

    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("showPersonForm")) {
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado valor en propiedad PEOPLE en persona-main");

            this.dispatchEvent(
                new CustomEvent("updated-people", {
                    detail: {
                        people: this.people
                    }
                })
            );
        }

        if (changedProperties.has("maxYearsInCompanyFilter")) {
            console.log("Ha cambiado valor en propiedad maxYearsInCompanyFilter en persona-main");
            console.log("Se van a mostrar las persona con antigüedad máxima de " + this.maxYearsInCompanyFilter + " años.");
        }
    }

    personFormClose() {
        console.log("personFormClose");

        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore");

        if (e.detail.editingPerson === true) {
            console.log("Se modifica existente " + e.detail.person.name);

            /* let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            
            if (indexOfPerson >= 0) {
                this.people[indexOfPerson] = e.detail.person;
            } */

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            );

        } else {
            console.log("Se añade nuevo");
            //this.people.push(e.detail.person);
            this.people = [...this.people, e.detail.person];

        }

        this.showPersonForm = false;
    }

    showPersonList() {
        console.log("showPersonList");
        this.shadowRoot.getElementById("idPersonForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("idPersonForm").classList.remove("d-none");
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);

        this.people = this.people.filter(
            person => person.name !== e.detail.name
        );
    }

    infoPerson(e) {
        console.log("infoPerson");
        console.log("Se ha pedido más info de " + e.detail.name);

        let choosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = choosenPerson[0].name;
        person.profile = choosenPerson[0].profile;
        person.yearsInCompany = choosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("idPersonForm").person = person;
        this.shadowRoot.getElementById("idPersonForm").editingPerson = true;
        this.showPersonForm = true;
    }

    setPeople(e) {
        console.log("setPeople");
        this.people = e.detail.people;
    }

}

customElements.define('persona-main', PersonaMain);