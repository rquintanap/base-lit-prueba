import { LitElement, html } from 'lit-element';

class PersonaDm extends LitElement{

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: "nombre 1",
                yearsInCompany: 10,
                profile: "ndsanlkadsnkladkdsa.",
                photo: {
                    src: "./img/img1.jpeg",
                    alt: "nombre 1"
                }
            },
            {
                name: "nombre 2",
                yearsInCompany: 2,
                profile: "oihfsdfdsfs.",
                photo: {
                    src: "./img/img2.jpeg",
                    alt: "nombre 2"
                }
            },
            {
                name: "nombre 3",
                yearsInCompany: 5,
                profile: "djksabndkjasndkjasndkjasnkjdas.",
                photo: {
                    src: "./img/img3.jpeg",
                    alt: "nombre 3"
                }
            },
            {
                name: "nombre 4",
                yearsInCompany: 7,
                profile: "pkpomomoninini.",
                photo: {
                    src: "./img/img4.png",
                    alt: "nombre 4"
                }
            },
            {
                name: "nombre 5",
                yearsInCompany: 9,
                profile: "biuydbaabsiud.",
                photo: {
                    src: "./img/img5.jpeg",
                    alt: "nombre 5"
                }
            }
        ];
    }

    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("people")) {
            console.log("changedProperties people");
            this.dispatchEvent(
                new CustomEvent("set-people", {
                    detail: {
                        people: this.people
                    }
                })
            );
        }
    }

}

customElements.define('persona-dm', PersonaDm);