import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement{

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}
        };
    }

    constructor() {
        super();

        this.name = "Prueba nombre";
        this.yearsInCompany = 11;
    }

    //Para saber cuándo ha cambiado el valor de una propiedad
    updated(changedProperties) {
        console.log("updated");

        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia de valor, el anterior era " + oldValue);
        });

        if (changedProperties.has("name")) {
            console.log("Propiedad name ha cambiado de valor, el anterior era " + changedProperties.get("name") + ", el nuevo es " + this.name);
        }

        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany ha cambiado de valor, el anterior era " + changedProperties.get("yearsInCompany") + ", el nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }

    render() {
        return html`
            <div>
                <label>Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `;
    }


    updateName(e) {
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo() {
        console.log("updatePersonInfo");
        if (this.yearsInCompany >= 7) {
            this.personInfo = "Lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "Senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "Team";
        } else {
            this.personInfo = "Junior";
        }
    }

}

customElements.define('ficha-persona', FichaPersona);